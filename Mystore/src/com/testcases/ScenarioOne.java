package com.testcases;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.baseclass.BaseClass;
import com.pom.ScenarioOnePom;

public class ScenarioOne extends BaseClass {

	@Test
	public static void myTest() throws InterruptedException, IOException {
		
		ScenarioOnePom pom=new ScenarioOnePom(driver);

		//Sign In
		pom.signClick();
		
		//Email Id
		pom.enterEmail();
		
		//Password
		pom.enterPass();

		//Click on Login
		pom.loginButtonClick();
		
		//Hover on the element
		Actions action = new Actions(driver);
		WebElement dresses=driver.findElement(By.xpath("/html/body/div[1]/div[1]/header/div[3]/div/div/div[6]/ul/li[2]/a"));
		action.moveToElement(dresses).moveToElement(driver.findElement(By.xpath("/html/body/div[1]/div[1]/header/div[3]/div/div/div[6]/ul/li[2]/ul/li[3]/a"))).click().build().perform();
		
		//Click on List view
		pom.listClick();
		
		//Check the stock and color and click on quickview
		WebElement stock=driver.findElement(By.xpath("//link[@href='http://schema.org/InStock']/../../preceding-sibling::div//li/a[@href='http://automationpractice.com/index.php?id_product=5&controller=product#/size-s/color-blue']"));
		
		if(stock.isDisplayed()) {
			
			Thread.sleep(2000);
			WebElement quickbView = driver.findElement(By.xpath("//link[@href='http://schema.org/InStock']/../../preceding-sibling::div//li/a[contains(@href,'color-blue')]//ancestor::div[contains(@class,'center-block')]/preceding-sibling::div//img[@src='http://automationpractice.com/img/p/1/2/12-home_default.jpg']"));
			action.moveToElement(quickbView).build().perform();
			Thread.sleep(2000);
			JavascriptExecutor js=(JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", quickbView);
		}
		
		//Facebook click
		pom.fackbook();
		
		//Window Handles 
		String parentWindow=driver.getWindowHandle();
		Set<String> allWindows=driver.getWindowHandles();
		
		for(String a:allWindows ) {
			if(!a.equals(parentWindow)) {
				driver.switchTo().window(a);
				
				//Facebook Title check
				Assert.assertTrue((driver.getTitle()).contains("Facebook"));
				
				//Close Facebook window
				driver.close();
			}
		
	
		}
		
		//Switch to parent window
		driver.switchTo().window(parentWindow);
		
		//Quantity
		pom.clickOnQuantity();
		
		//Wishlist
		pom.clickWishlist();
		
		//Close Wishlist
		//a[@title='Close']
		//a[contains(@class,'fancybox-item fancybox-close')]
		
		Thread.sleep(5000);
		WebElement closeWishlist=driver.findElement(By.xpath("//a[contains(@class,'fancybox-item fancybox-close')]"));
		JavascriptExecutor js1=(JavascriptExecutor)driver;
		js1.executeScript("arguments[0].click();", closeWishlist);
		
		//Add to cart
		pom.addToCart();
		
		//Price check
		Thread.sleep(2000);
		WebElement Total=driver.findElement(By.id("layer_cart_product_price"));
		Assert.assertEquals(Total.getText(), "$57.96");
		
		//Proceed to checkout
		WebElement proceedToCheckout1=driver.findElement(By.xpath("//span[contains(text(),'Proceed to checkout')]"));
		proceedToCheckout1.click();
		
		//Delivery Address check
		WebElement deliveryAddress=driver.findElement(By.xpath("//h3[contains(text(),'Delivery address')]/../following-sibling::li//span[contains(text(),'Amogha')]"));
		Thread.sleep(2000);
		Assert.assertTrue(deliveryAddress.isDisplayed());
		
		//Product check
		WebElement product=driver.findElement(By.xpath("(//p[@class='product-name']/a[contains(text(),'Printed Summer Dress')])[2]"));
		Thread.sleep(2000);
		Assert.assertTrue(product.isDisplayed());
		
		//Price check
		WebElement price=driver.findElement(By.id("total_price"));
		Thread.sleep(2000);
		Assert.assertEquals(price.getText(), "$59.96");
		
		//Proceed to checkout
		Thread.sleep(2000);
		WebElement proceedToCheckout2=driver.findElement(By.xpath("(//span[contains(text(),'Proceed to checkout')])[2]"));
		proceedToCheckout2.click();
		
		//Proceed to checkout
		Thread.sleep(2000);
		WebElement proceedToCheckout3=driver.findElement(By.xpath("(//span[contains(text(),'Proceed to checkout')])[2]"));
		proceedToCheckout3.click();
		
		// click on checkbox
		Thread.sleep(2000);
		WebElement checkbook=driver.findElement(By.id("cgv"));
		checkbook.click();
		
		//Proceed to checkout
		WebElement proceedToCheckout4=driver.findElement(By.xpath("(//span[contains(text(),'Proceed to checkout')])[2]"));
		proceedToCheckout4.click();
		
		//Total price check
		WebElement totalPrice=driver.findElement(By.id("total_price"));
		Thread.sleep(2000);
		Assert.assertEquals(totalPrice.getText(), "$59.96");
		
		//View my Customer account
		Thread.sleep(2000);
		WebElement viewMyCustomerAccount =driver.findElement(By.xpath("//span[text()='Amogha N J']"));
		viewMyCustomerAccount.click();
		
		//Final Wishlist
		Thread.sleep(2000);
		WebElement finalWishlist =driver.findElement(By.xpath("//span[text()='My wishlists']"));
		finalWishlist.click();
		
		//Final Product and check
		Thread.sleep(2000);
		WebElement finalProduct =driver.findElement(By.xpath("(//a[contains(text(),'Printed Summer Dress')])[1]"));
		Assert.assertTrue(finalProduct.isDisplayed());
	
		driver.quit();
			
	}
}
