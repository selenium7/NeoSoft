package com.baseclass;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;

public class BaseClass {

	protected static WebDriver driver;
	protected static WebDriverWait wait;
	
	@BeforeTest
	public static void myDriver() {

		//Set Property chrome driver
		System.setProperty("webdriver.chrome.driver", "./Library/chromedriver.exe");
		
		//Open chrome driver
		driver = new ChromeDriver();

		//URL
		driver.get("http://automationpractice.com/index.php");
		
		//To Maximize window
		driver.manage().window().maximize();
		
	}
	
	//Read properties file data
	public String getData(String text) throws IOException {
		FileInputStream fis=new FileInputStream("./data.properties");
		Properties data=new Properties();
		data.load(fis);
		return data.getProperty(text);
	}
	
	//Wait for the element
	public void waitForTheElement(WebElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));	
	}
	
	//Send keys to the element
	public void sendKeysForElement(WebElement element, String text) {
		waitForTheElement(element);
		element.sendKeys(text);
	}
	
	//click for element
	public void clickForElement(WebElement element) {
		waitForTheElement(element);
		element.click();
	}
	
	
	
}
