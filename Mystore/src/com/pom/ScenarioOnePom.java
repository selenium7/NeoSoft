package com.pom;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.baseclass.BaseClass;

public class ScenarioOnePom extends BaseClass {

	public ScenarioOnePom(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(className = "login")
	private WebElement signIn;

	@FindBy(id = "email")
	private WebElement emailID;

	@FindBy(id = "passwd")
	private WebElement pass;

	@FindBy(id = "SubmitLogin")
	private WebElement button;

	@FindBy(xpath = "//li[@id='list']/a/i")
	private WebElement listView;

	@FindBy(xpath = "//button[contains(@class,'facebook')]")
	private WebElement fb;

	@FindBy(id = "quantity_wanted")
	private WebElement quantity;

	@FindBy(id = "wishlist_button")
	private WebElement wishlist;

	@FindBy(id = "add_to_cart")
	private WebElement cart;
	
	
	public void signClick() {
		clickForElement(signIn);
	}

	public void enterEmail() throws IOException {

		sendKeysForElement(emailID, getData("email"));
	}

	public void enterPass() throws IOException {

		sendKeysForElement(pass, getData("pass"));
	}

	public void loginButtonClick() {
		clickForElement(button);
	}

	public void listClick() {
		clickForElement(listView);
	}

	public void fackbook() {
		clickForElement(fb);
	}

	public void clickOnQuantity() throws IOException {
		quantity.clear();
		sendKeysForElement(quantity, getData("quantity"));
	}

	public void clickWishlist() {
		clickForElement(wishlist);
	}
	
	public void addToCart() {
		clickForElement(cart);
	}

}
